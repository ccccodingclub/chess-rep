package ccc.chess;

import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.managers.*;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.Random;

public class Chess extends Application {

    private BotManager botManager;
    private GUIManager  guiManager;
    private GameManager gameManager;
    private SoundManager soundManager;
    private ConnectionManager connManager;

    private int         WINDOW_WIDTH;
    private int         WINDOW_HEIGHT;
    private float       scale_factor = 0.75f;
    private double      volume = 0.50D;
    private StackPane   masterPane;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() {
        // Load anything before launch
        Font.loadFont(getClass().getClassLoader().getResourceAsStream("fonts/KGHAPPY.ttf"), 16);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // Setup Frame Size
        double screenWidth = Screen.getPrimary().getBounds().getWidth();
        double screenHeight = Screen.getPrimary().getBounds().getHeight();
        double least = (screenWidth < screenHeight) ? screenWidth : screenHeight;
        this.WINDOW_WIDTH = (int) (least * scale_factor);
        this.WINDOW_HEIGHT = (int) (least * scale_factor);

        // Set Stage Properties
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/chess_icon.png")));
        stage.setTitle("Chess");
        StackPane masterPane = new StackPane();
        stage.setScene(new Scene(masterPane, WINDOW_WIDTH, WINDOW_HEIGHT));
        stage.getScene().getStylesheets().add(getClass().getResource("/stylesheets/style.css").toString());
        this.masterPane = masterPane;

        // Setup Managers
        this.gameManager = new GameManager(this);
        this.guiManager = new GUIManager(this);
        this.guiManager.setActivePanel(PanelType.MENU, TransitionType.NONE_OUT, TransitionType.NONE_IN);
        this.soundManager = new SoundManager(this);
        this.connManager = new ConnectionManager(this);
        this.botManager = new BotManager(this);

        // Make frame visible
        stage.sizeToScene();
        stage.show();

        // Start game loop
        this.loop();
    }

    @Override
    public void stop() {
        Platform.exit();
        getConnManager().closeConnection();
    }

    private void loop() {
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (connManager.getClient() == null) {
                    if (!gameManager.isTurn()) {
                        if (getGameManager().isInGame()) {
                            getBotManager().bot();
                        }
                    }
                }
            }
        }.start();
    }

    public BotManager getBotManager() {
        return botManager;
    }

    public GUIManager getGUIManager() {
        return guiManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public ConnectionManager getConnManager() {
        return connManager;
    }

    public StackPane getMasterPane() {
        return masterPane;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

}
