package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

import java.util.function.UnaryOperator;

public class HostPanel extends ChessPanel {

    public HostPanel(Chess main) {
        super(main, PanelType.HOST);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("HOST"));
        banner.setAlignment(Pos.CENTER);

        Button host = new Button("HOST");
        host.setDisable(true);

        TextField portField = new TextField();
        portField.setPromptText("PORT #");
        portField.setMaxWidth(getWidth() / 1.5);
        String portRegex = "^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$";
        UnaryOperator<TextFormatter.Change> portFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches(portRegex) || newText.equals("")) {
                return change;
            }
            return null;
        };
        portField.setTextFormatter(new TextFormatter<>(portFilter));
        portField.setOnKeyReleased(event -> host.setDisable(!portField.getText().matches(portRegex)));

        host.setId("button-ok");
        host.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            int portNum = Integer.parseInt(portField.getText());
            main.getConnManager().hostConnection(portNum);
            main.getGUIManager().setInfoText("Waiting for opponent...");
            main.getGUIManager().setActivePanel(PanelType.INFO, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
        });

        Button back = new Button("BACK");
        back.setId("button-warn");
        back.getStyleClass().add("button-lg");
        back.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.PLAY, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
        });

        VBox content = new VBox(getHeight() / 50);
        content.setStyle("-fx-padding: 30 0 0 0;");
        content.getChildren().add(portField);
        content.getChildren().add(host);
        content.getChildren().add(back);
        content.setAlignment(Pos.TOP_CENTER);

        setTop(banner);
        setCenter(content);
    }

    @Override
    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                main.getGUIManager().setActivePanel(PanelType.PLAY, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
            }
        });
    }
}
