package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.animation.AnimationTimer;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class InfoPanel extends ChessPanel {

    private long lastFrame;

    public InfoPanel(Chess main) {
        super(main, PanelType.INFO);
    }

    @Override
    public void init() {
        Label info = new Label();
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                if ((now - lastFrame) / 1000000.0 >= 10L) { // Lock at 60FPS
                    lastFrame = now;
                    info.setText(main.getGUIManager().getInfoText());
                }
            }
        }.start();
        info.getStyleClass().add("label-sm");

        Button back = new Button("BACK");
        back.setId("button-warn");
        back.getStyleClass().add("button-lg");
        back.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getConnManager().closeConnection();
            main.getGUIManager().setActivePanel(PanelType.PLAY, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
        });

        VBox content = new VBox();
        content.getChildren().add(info);
        content.setAlignment(Pos.CENTER);

        VBox footer = new VBox();
        footer.getChildren().add(back);
        footer.setAlignment(Pos.TOP_CENTER);

        setCenter(content);
        setBottom(footer);
    }
}
