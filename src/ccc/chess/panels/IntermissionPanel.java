package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class IntermissionPanel extends ChessPanel {
    public IntermissionPanel(Chess main) {
        super(main, PanelType.INTERMISSION);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        String status = (main.getGameManager().won()) ? "YOU WON!" : "YOU LOSE!";
        banner.getChildren().add(new Label(status));
        Label hint = new Label("[ Press ESC to view the board! ]");
        hint.getStyleClass().add("label-sm");
        banner.getChildren().add(hint);
        banner.setAlignment(Pos.CENTER);

        Button playAgain = new Button("Play Again");
        playAgain.setId("button-ok");
        playAgain.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setInfoText("Waiting for Opponent...");
            main.getGUIManager().setActivePanel(PanelType.INFO, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
            main.getGUIManager().getBackgroundPanel().setBlurred(true);

            if (main.getGameManager().isRematch() || main.getConnManager().getClient() == null) {
                main.getGameManager().initBoard(main.getGameManager().getTeam());
                main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                main.getGUIManager().getBackgroundPanel().setBlurred(false);
            } else {
                main.getGameManager().setRematch(true);
            }

            if (main.getConnManager().getClient() != null) {
                try {
                    PrintWriter out = new PrintWriter(main.getConnManager().getClient().getOutputStream(), true);
                    String header = "REMATCH";
                    out.println(header);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        Button quit = new Button("Quit");
        quit.setId("button-warn");
        quit.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
            main.getGUIManager().getBackgroundPanel().setBlurred(true);
            if (main.getConnManager().getClient() != null) {
                main.getConnManager().closeConnection();
            }
        });

        HBox content = new HBox(getHeight() / 30);
        content.setStyle("-fx-padding: 30 0 0 0;");
        content.getChildren().add(playAgain);
        content.getChildren().add(quit);
        content.setAlignment(Pos.CENTER);

        setTop(banner);
        setCenter(content);
    }

    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                main.getGUIManager().getBackgroundPanel().setBlurred(false);
            }
        });
    }
}
