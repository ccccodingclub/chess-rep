package ccc.chess.panels.core;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import javafx.animation.Animation;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

@SuppressWarnings("WeakerAccess")
public abstract class ChessPanel extends BorderPane {

    public  Chess       main;
    private PanelType   type;
    private Animation   currentAnimation;

    public ChessPanel(Chess main, PanelType type) {
        this.main = main;
        this.type = type;
        this.setStyle("-fx-background-color: rgba(200, 200, 200, 0.25);");
        this.setVisible(false);
        this.setOpacity(1.0F);
        this.setWidth(main.getMasterPane().getWidth());
        this.setHeight(main.getMasterPane().getHeight());
        this.setCache(true);
        this.setCacheShape(true);
        this.setCacheHint(CacheHint.SPEED);
    }

    public abstract void init();

    public void setupKeyHandler() { }

    public void setupMouseHandler() { }

    public PanelType getType() {
        return type;
    }

    public Animation getCurrentAnimation() {
        return currentAnimation;
    }

    public void setCurrentAnimation(Animation currentAnimation) {
        this.currentAnimation = currentAnimation;
    }

    public boolean animationsDone() {
        return getCurrentAnimation() != null
                && getCurrentAnimation().getStatus() == Animation.Status.STOPPED;
    }
}
