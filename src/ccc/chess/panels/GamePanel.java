package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.managers.GUIManager;
import ccc.chess.objects.Piece;
import ccc.chess.objects.Tile;
import ccc.chess.panels.core.ChessPanel;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GamePanel extends ChessPanel {

    private Canvas          canvas;
    private GraphicsContext gc;
    private boolean         l337h4x;
    private List<KeyCode>   lastKeysTyped;
    private int             hue1;
    private int             hue2;

    private static final KeyCode[] konami = new KeyCode[] {
            KeyCode.UP,
            KeyCode.UP,
            KeyCode.DOWN,
            KeyCode.DOWN,
            KeyCode.LEFT,
            KeyCode.RIGHT,
            KeyCode.LEFT,
            KeyCode.RIGHT,
            KeyCode.B,
            KeyCode.A,
    };

    public GamePanel(Chess main) {
        super(main, PanelType.GAME);
        this.setStyle("");
        this.canvas = new Canvas(getWidth(), getHeight());
        this.gc = canvas.getGraphicsContext2D();
        this.lastKeysTyped = new ArrayList<>();
    }

    @Override
    public void init() {
        this.setCenter(canvas);
        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                // Make sure this is the active panel
                if (main.getGUIManager().getActivePanel() != null
                        && main.getGUIManager().getActivePanel().getType() == PanelType.GAME) {
                    // Clear before frame
                    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

                    // Draw l337h4x
                    if (l337h4x) {
                        hue1 = (hue1 == 360) ? 0 : hue1 + 1;
                        hue2 = (hue2 == 0) ? 360 : hue2 - 1;
                        Stop[] stops = new Stop[]{new Stop(0, Color.hsb(hue1, 0.5, 0.5, 0.6)), new Stop(1, Color.hsb(hue2, 0.5, 0.5, 0.6))};
                        gc.setFill(new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops));
                        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
                    }

                    // Draw Box @ Selected Tile
                    if (main.getGUIManager().getSelectedTile() != null) {
                        if (main.getGameManager().getPiece(main.getGUIManager().getSelectedTile()) == null) {
                            gc.setFill(new Color(0, 0, 0, getOpacity() / 2));
                        } else if (main.getGameManager().getPiece(main.getGUIManager().getSelectedTile()).isControllable()) {
                            gc.setFill(new Color(0, 1.0, 0, getOpacity() / 2));
                        } else {
                            gc.setFill(new Color(1.0, 0, 0, getOpacity() / 2));
                        }
                        gc.fillRect(main.getGUIManager().getSelectedTile().getX(),
                                main.getGUIManager().getSelectedTile().getY(),
                                main.getGUIManager().getSelectedTile().getWidth(),
                                main.getGUIManager().getSelectedTile().getHeight());

                        // Draw Selected Tile's Possible Moves
                        gc.setFill(new Color(1.0, 1.0, 0, getOpacity() / 4));
                        Piece piece = main.getGameManager().getPiece(main.getGUIManager().getSelectedTile());
                        if (piece != null) {
                            for (Tile tile : piece.getPossibleMoves()) {
                                gc.fillRect(tile.getX(), tile.getY(), tile.getWidth(), tile.getHeight());
                            }
                        }
                    }

                    // Draw Box @ Hovered Tile
                    gc.setFill(new Color(0, 1.0, 0, getOpacity() / 4));
                    if (main.getGUIManager().getHoveredTile() != null) {
                        if (main.getGameManager().getPiece(main.getGUIManager().getHoveredTile()) == null) {
                            gc.setFill(new Color(0, 0, 0, getOpacity() / 4));
                        } else if (main.getGameManager().getPiece(main.getGUIManager().getHoveredTile()).isControllable()) {
                            gc.setFill(new Color(0, 1.0, 0, getOpacity() / 4));
                        } else {
                            gc.setFill(new Color(1.0, 0, 0, getOpacity() / 4));
                        }
                        gc.fillRect(main.getGUIManager().getHoveredTile().getX(),
                                main.getGUIManager().getHoveredTile().getY(),
                                main.getGUIManager().getHoveredTile().getWidth(),
                                main.getGUIManager().getHoveredTile().getHeight());
                    }

                    // Draw rectangles at last-move position
                    if (main.getGameManager().getLastMove().size() > 0) {
                        Tile t1 = main.getGameManager().getLastMove().get(0);
                        Tile t2 = main.getGameManager().getLastMove().get(1);

                        for (int x = 0; x < 10; x++) {
                            gc.setLineWidth(1.0 - ((float) x / 10.0));
                            gc.setStroke(new Color(0, 0, 1.0, 1.0));
                            gc.strokeRect(t1.getX() + (3 * x), t1.getY() + (3 * x),
                                    (t1.getWidth() - 1) - (6 * x), (t1.getHeight() - 1) - (6 * x));
                            gc.strokeRect(t2.getX() + (3 * x), t2.getY() + (3 * x),
                                    (t2.getWidth() - 1) - (6 * x), (t2.getHeight() - 1) - (6 * x));
                        }
                    }

                    // Draw Chess Pieces (last)
                    for (Piece piece : main.getGameManager().getAllPieces()) {

                        // Highlight (Warn) when king is on check!
                        if (piece.isKing() && main.getGameManager().checkOwnCheck(piece, main.getGUIManager().getBoardTiles()[piece.getTile().getIndex()])) {
                            gc.setFill(new Color(1.0, 0, 0, 0.25));
                            gc.fillRect((int) piece.getNewLocation().getX(), (int) piece.getNewLocation().getY(),
                                    main.getGUIManager().getTileWidth(), main.getGUIManager().getTileHeight());
                            for (int x = 0; x < 10; x++) {
                                gc.setLineWidth(1.0 - ((float) x / 10.0));
                                gc.setStroke(new Color(1.0, 0, 0, 1.0));
                                gc.strokeRect(piece.getTile().getX() + (3 * x), piece.getTile().getY() + (3 * x),
                                        (piece.getTile().getWidth() - 1) - (6 * x), (piece.getTile().getHeight() - 1) - (6 * x));
                            }
                        }

                        /* Slide the current piece to new location */
                        int rise = (int) Math.abs((piece.getNewLocation().getY() - piece.getLocation().getY()) / 5) + 1;
                        int run = (int) Math.abs((piece.getNewLocation().getX() - piece.getLocation().getX()) / 5) + 1;

                        if (piece.getNewLocation().getX() < piece.getLocation().getX()) {
                            piece.setLocation((int) (piece.getLocation().getX() - run), (int) (piece.getLocation().getY()));
                        } else if (piece.getNewLocation().getX() > piece.getLocation().getX()) {
                            piece.setLocation((int) (piece.getLocation().getX() + run), (int) (piece.getLocation().getY()));
                        }
                        if (piece.getNewLocation().getY() < piece.getLocation().getY()) {
                            piece.setLocation((int) (piece.getLocation().getX()), (int) (piece.getLocation().getY() - rise));
                        } else if (piece.getNewLocation().getY() > piece.getLocation().getY()) {
                            piece.setLocation((int) (piece.getLocation().getX()), (int) (piece.getLocation().getY() + rise));
                        }

                        if (main.getGameManager().getTeam() == piece.getColor() && main.getGameManager().isTurn()) {
                            gc.setGlobalAlpha(1.0F);
                        } else {
                            gc.setGlobalAlpha(0.6F);
                        }
                        gc.drawImage(GUIManager.getPieceImage(piece.getType()),
                                (int) piece.getLocation().getX(), (int) piece.getLocation().getY(),
                                main.getGUIManager().getTileWidth(), main.getGUIManager().getTileWidth());
                    }
                }
            }
        }.start();
    }

    public void setupMouseHandler() {
        this.setOnMouseMoved(event -> {
            for (Tile tile : main.getGUIManager().getBoardTiles()) {
                if (new Rectangle(tile.getX(), tile.getY(), tile.getWidth(), tile.getHeight())
                        .contains(event.getX(), event.getY())) {
                    main.getGUIManager().setHoveredTile(tile);
                }
            }
        });
        this.setOnMouseExited(event -> {
            if (main.getGUIManager().getHoveredTile() != null) {
                main.getGUIManager().setHoveredTile(null);
            }
        });
        this.setOnMousePressed(event -> {
            for (Tile tile : main.getGUIManager().getBoardTiles()) {
                if (new Rectangle(tile.getX(), tile.getY(), tile.getWidth(), tile.getHeight())
                        .contains(event.getX(), event.getY())) {

                    Piece piece = main.getGameManager().getPiece(tile);
                    if (piece != null && piece.isControllable()) {
                        main.getGUIManager().setSelectedTile(tile);
                    }

                    if (main.getGameManager().isInGame() && main.getGameManager().isTurn()) {
                        if (main.getGUIManager().getSelectedTile() != null) {
                            Piece selectedPiece = main.getGameManager().getPiece(main.getGUIManager().getSelectedTile());
                            if (selectedPiece.getPossibleMoves().contains(tile)) {
                                if (!main.getGameManager().checkOwnCheck(selectedPiece, tile)) {
                                    // Send Moves over Connection
                                    if (main.getConnManager().getClient() != null) {
                                        try {
                                            PrintWriter out = new PrintWriter(main.getConnManager().getClient().getOutputStream(), true);
                                            String header = "MOVE";
                                            int index1 = selectedPiece.getTile().getIndex();
                                            int index2 = Arrays.asList(main.getGUIManager().getBoardTiles()).indexOf(tile);
                                            out.println(String.join(":", new String[]{header, String.valueOf(index1), String.valueOf(index2)}));
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }
                                    }

                                    // Send Moves to Client
                                    main.getGameManager().movePiece(selectedPiece, tile);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                if (main.getGameManager().isInGame())
                    main.getGUIManager().setActivePanel(PanelType.PAUSE, TransitionType.FADE_OUT, TransitionType.FADE_IN);
                else
                    main.getGUIManager().setActivePanel(PanelType.INTERMISSION, TransitionType.FADE_OUT, TransitionType.FADE_IN);
                main.getGUIManager().getBackgroundPanel().setBlurred(true);
            }

            lastKeysTyped.add(event.getCode());

            if (lastKeysTyped.size() > 10) {
                lastKeysTyped.remove(0);
            }

            if (Arrays.equals(lastKeysTyped.toArray(), konami))
                l337h4x = !l337h4x;
        });
    }
}
