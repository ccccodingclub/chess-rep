package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class SettingsPanel extends ChessPanel {

    public SettingsPanel(Chess main) {
        super(main, PanelType.SETTINGS);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("SETTINGS"));
        banner.setAlignment(Pos.CENTER);

        Label volumeLbl = new Label("VOLUME");
        volumeLbl.getStyleClass().add("label-sm");

        ProgressBar pb = new ProgressBar(0);
        ProgressIndicator pi = new ProgressIndicator(0);

        Slider volumeSlider = new Slider(0.0, 1.0, main.getVolume());
        volumeSlider.valueProperty().addListener(event -> {
            main.setVolume(volumeSlider.getValue());
            pb.setProgress(volumeSlider.getValue());
            pi.setProgress(volumeSlider.getValue());
        });
        volumeSlider.setOnMouseReleased(event -> main.getSoundManager().playSound("menu_click.mp3"));
        volumeSlider.setMaxWidth(getWidth() / 2);

        pb.setProgress(volumeSlider.getValue());
        pi.setProgress(volumeSlider.getValue());
        pb.setMinWidth(volumeSlider.getMaxWidth() - 10);

        StackPane slider = new StackPane();
        slider.getChildren().addAll(pb, volumeSlider);

        VBox content = new VBox(getHeight() / 50);
        content.getChildren().add(volumeLbl);
        content.getChildren().add(slider);
        content.setAlignment(Pos.TOP_CENTER);

        Button back = new Button("BACK");
        back.setId("button-warn");
        back.getStyleClass().add("button-lg");
        back.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
        });

        VBox footer = new VBox();
        footer.getChildren().add(back);
        footer.setAlignment(Pos.TOP_CENTER);

        setTop(banner);
        setCenter(content);
        setBottom(footer);
    }

    @Override
    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
            }
        });
    }
}
