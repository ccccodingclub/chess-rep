package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

import java.util.function.UnaryOperator;

public class JoinPanel extends ChessPanel {

    public JoinPanel(Chess main) {
        super(main, PanelType.JOIN);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("JOIN"));
        banner.setAlignment(Pos.CENTER);

        Button connect = new Button("CONNECT");
        connect.setDisable(true);
        TextField addressField = new TextField();
        TextField portField = new TextField();

        String portRegex = "^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$";

        addressField.setPromptText("HOSTNAME");
        addressField.setMaxWidth(getWidth() / 1.5);
        addressField.setOnKeyReleased(event -> {
            boolean validInput = !addressField.getText().equalsIgnoreCase("")
                    && portField.getText().matches(portRegex);
            connect.setDisable(!validInput);
        });

        portField.setPromptText("PORT #");
        portField.setMaxWidth(getWidth() / 1.5);
        UnaryOperator<TextFormatter.Change> portFilter = change -> {
            String newText = change.getControlNewText();
            return (newText.matches(portRegex) || newText.equals("")) ? change : null;
        };
        portField.setTextFormatter(new TextFormatter<>(portFilter));
        portField.setOnKeyReleased(event -> {
            boolean validInput = !addressField.getText().equalsIgnoreCase("")
                    && portField.getText().matches(portRegex);
            connect.setDisable(!validInput);
        });

        connect.setId("button-ok");
        connect.setOnMouseClicked(event -> {
            String address = addressField.getText();
            int portNum = Integer.parseInt(portField.getText());

            main.getSoundManager().playSound("menu_click.mp3");
            main.getConnManager().joinConnection(address, portNum, false);
            main.getGUIManager().setInfoText("Connecting...");
            main.getGUIManager().setActivePanel(PanelType.INFO, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
        });

        Button back = new Button("BACK");
        back.setId("button-warn");
        back.getStyleClass().add("button-lg");
        back.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.PLAY, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
        });

        VBox content = new VBox(5);
        content.setStyle("-fx-padding: 30 0 0 0;");
        content.getChildren().add(addressField);
        content.getChildren().add(portField);
        content.getChildren().add(connect);
        content.getChildren().add(back);
        content.setAlignment(Pos.TOP_CENTER);

        setTop(banner);
        setCenter(content);
    }

    @Override
    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                main.getGUIManager().setActivePanel(PanelType.PLAY, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
            }
        });
    }
}
