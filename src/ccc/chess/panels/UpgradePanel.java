package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.PieceType;
import ccc.chess.enums.TransitionType;
import ccc.chess.managers.GUIManager;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class UpgradePanel extends ChessPanel {

    public UpgradePanel(Chess main) {
        super(main, PanelType.UPGRADE);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("CHOOSE UPGRADE"));
        banner.setAlignment(Pos.BOTTOM_CENTER);

        if (main.getGameManager().getPromotePiece() != null) {
            boolean team = main.getGameManager().getPromotePiece().getColor();

            HBox content = new HBox(getWidth() / 30);

            Button rook = new Button();
            rook.setGraphic(new ImageView(GUIManager.getPieceImage((team) ? PieceType.WHITE_ROOK : PieceType.BLACK_ROOK)));
            rook.setOnMouseClicked(event -> {
                main.getSoundManager().playSound("menu_click.mp3");
                doUpgrade((team) ? PieceType.WHITE_ROOK : PieceType.BLACK_ROOK);
            });

            Button knight = new Button();
            knight.setGraphic(new ImageView(GUIManager.getPieceImage((team) ? PieceType.WHITE_KNIGHT : PieceType.BLACK_KNIGHT)));
            knight.setOnMouseClicked(event -> {
                main.getSoundManager().playSound("menu_click.mp3");
                doUpgrade((team) ? PieceType.WHITE_KNIGHT : PieceType.BLACK_KNIGHT);
            });

            Button bishop = new Button();
            bishop.setGraphic(new ImageView(GUIManager.getPieceImage((team) ? PieceType.WHITE_BISHOP : PieceType.BLACK_BISHOP)));
            bishop.setOnMouseClicked(event -> {
                main.getSoundManager().playSound("menu_click.mp3");
                doUpgrade((team) ? PieceType.WHITE_BISHOP : PieceType.BLACK_BISHOP);
            });

            Button queen = new Button();
            queen.setGraphic(new ImageView(GUIManager.getPieceImage((team) ? PieceType.WHITE_QUEEN : PieceType.BLACK_QUEEN)));
            queen.setOnMouseClicked(event -> {
                main.getSoundManager().playSound("menu_click.mp3");
                doUpgrade((team) ? PieceType.WHITE_QUEEN : PieceType.BLACK_QUEEN);
            });

            rook.setId("button-promote");
            knight.setId("button-promote");
            bishop.setId("button-promote");
            queen.setId("button-promote");
            content.getChildren().addAll(rook, knight, bishop, queen);
            content.setStyle("-fx-padding: 30 0 0 0;");
            content.setAlignment(Pos.TOP_CENTER);

            setTop(banner);
            setCenter(content);
        }
    }

    private void doUpgrade(PieceType type) {
        main.getGameManager().promote(type);
        main.getGUIManager().setSelectedTile(null);
        main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN);
        try {
            PrintWriter out = new PrintWriter(main.getConnManager().getClient().getOutputStream(), true);
            String header = "PROMOTION";
            out.println(String.join(":", new String[]{ header, type.name() }));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
