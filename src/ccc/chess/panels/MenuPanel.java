package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class MenuPanel extends ChessPanel {

    public MenuPanel(Chess main) {
        super(main, PanelType.MENU);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("♚ CHESS ♛"));
        banner.setAlignment(Pos.CENTER);

        Button play = new Button("PLAY");
        play.setId("button-ok");
        play.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGameManager().initBoard(true);
            main.getGUIManager().setActivePanel(PanelType.PLAY, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_RIGHT);
        });

        Button settings = new Button("SETTINGS");
        settings.setId("button-ok");
        settings.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.SETTINGS, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_RIGHT);
        });

        Button quit = new Button("QUIT");
        quit.setId("button-warn");
        quit.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            Platform.exit();
        });

        VBox content = new VBox();
        content.setStyle("-fx-padding: 30 0 0 0;");
        content.getChildren().add(play);
        content.getChildren().add(settings);
        content.getChildren().add(quit);
        content.setAlignment(Pos.TOP_CENTER);

        setTop(banner);
        setCenter(content);
    }
}
