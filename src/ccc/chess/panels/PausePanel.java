package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class PausePanel extends ChessPanel {

    public PausePanel(Chess main) {
        super(main, PanelType.PAUSE);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("PAUSED"));
        banner.setAlignment(Pos.CENTER);

        Button resume = new Button("RESUME");
        resume.setId("button-ok");
        resume.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
            main.getGUIManager().getBackgroundPanel().setBlurred(false);
        });

        Button quit = new Button("QUIT");
        quit.setId("button-warn");
        quit.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
            main.getConnManager().closeConnection();
        });

        HBox content = new HBox(getWidth() / 30);
        content.setStyle("-fx-padding: 30 0 0 0;");
        content.getChildren().add(resume);
        content.getChildren().add(quit);
        content.setAlignment(Pos.CENTER);

        setTop(banner);
        setCenter(content);
    }

    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                main.getGUIManager().getBackgroundPanel().setBlurred(false);
            }
        });
    }
}
