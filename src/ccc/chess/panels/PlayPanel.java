package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.TransitionType;
import ccc.chess.panels.core.ChessPanel;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

import java.util.Random;

public class PlayPanel extends ChessPanel {

    public PlayPanel(Chess main) {
        super(main, PanelType.PLAY);
    }

    @Override
    public void init() {
        VBox banner = new VBox();
        banner.getChildren().add(new Label("PLAY"));
        banner.setAlignment(Pos.CENTER);

        Button host = new Button("HOST GAME");
        host.setId("button-ok");
        host.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.HOST, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_RIGHT);
        });

        Button join = new Button("JOIN GAME");
        join.setId("button-ok");
        join.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.JOIN, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_RIGHT);
        });

        Button start = new Button("START GAME");
        start.setId("button-ok");
        start.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGameManager().initBoard(new Random().nextBoolean());
            main.getGameManager().setInGame(true);
            main.getGUIManager().setSelectedTile(null);
            main.getGUIManager().getBackgroundPanel().setBlurred(!main.getGUIManager().getBackgroundPanel().isBlurred());
            main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN);
        });

        Button back = new Button("BACK");
        back.setId("button-warn");
        back.getStyleClass().add("button-lg");
        back.setOnMouseClicked(event -> {
            main.getSoundManager().playSound("menu_click.mp3");
            main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
        });

        VBox content = new VBox();
        content.setStyle("-fx-padding: 30 0 0 0;");
        content.getChildren().add(host);
        content.getChildren().add(join);
        content.getChildren().add(start);
        content.getChildren().add(back);
        content.setAlignment(Pos.TOP_CENTER);

        setTop(banner);
        setCenter(content);
    }

    @Override
    public void setupKeyHandler() {
        this.setOnKeyPressed(event -> {
            if (!animationsDone()) return;
            if (event.getCode() == KeyCode.ESCAPE) {
                main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
            }
        });
    }
}
