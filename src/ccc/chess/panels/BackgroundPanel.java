package ccc.chess.panels;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.panels.core.ChessPanel;
import javafx.animation.AnimationTimer;
import javafx.scene.CacheHint;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.transform.Scale;

import java.util.ArrayList;
import java.util.List;

public class BackgroundPanel extends ChessPanel {

    private double IMG_WIDTH;
    private double IMG_HEIGHT;
    private Image boardImage;
    private List<ImageView> frames;

    /* Blur Animation Stuff */
    private static final int    MAX_BLUR_RAD    = 20;
    private static final int    SCALE_SPEED     = 5;
    private static final int    BLUR_SPEED      = 2;
    private boolean             blurred         = true;
    private int                 curBlur         = 0;

    public BackgroundPanel(Chess main) {
        super(main, PanelType.BACKGROUND);
        this.setStyle("-fx-background-color: rgb(0, 0, 0);");
        this.setVisible(true);
        this.IMG_WIDTH   = main.getMasterPane().getWidth();
        this.IMG_HEIGHT  = main.getMasterPane().getHeight();
        this.frames      = new ArrayList<>();
        this.boardImage = new Image(getClass().getResourceAsStream("/images/board.png"));
        this.loadFrames();
        this.setCenter(frames.get(curBlur));
        this.setBlurred(true);
    }

    @Override
    public void init() {
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                int prevBlur = curBlur;
                if (blurred) {
                    curBlur = (curBlur + BLUR_SPEED < MAX_BLUR_RAD) ? curBlur + BLUR_SPEED : MAX_BLUR_RAD;
                } else {
                    curBlur = (curBlur - BLUR_SPEED > 0) ? curBlur - BLUR_SPEED : 0;
                }
                if (prevBlur != curBlur) {
                    setCenter(frames.get(curBlur));
                }
            }
        }.start();
    }

    public void setBlurred(boolean blurred) {
        this.blurred = blurred;
    }

    public boolean isBlurred() {
        return blurred;
    }

    private void loadFrames() {
        for (int x = 0; x < MAX_BLUR_RAD + 1; x++) {
            GaussianBlur blurEffect = new GaussianBlur(x);
            double newScale = 1 - (blurEffect.getRadius() / (SCALE_SPEED * MAX_BLUR_RAD));
            newScale = (newScale <= 0.8F) ? 0.8F : newScale;

            ImageView image = new ImageView(boardImage);
            image.setFitWidth(IMG_WIDTH);
            image.setFitHeight(IMG_HEIGHT);

            Scale imageScale = new Scale();
            imageScale.setPivotX(IMG_WIDTH / 2);
            imageScale.setPivotY(IMG_HEIGHT / 2);
            image.getTransforms().add(imageScale);

            imageScale.setX(newScale);
            imageScale.setY(newScale);

            image.setEffect(blurEffect);
            image.setCache(true);
            image.setCacheHint(CacheHint.SPEED);
            frames.add(image);
        }
    }
}
