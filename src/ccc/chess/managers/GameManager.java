package ccc.chess.managers;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.PieceType;
import ccc.chess.enums.TransitionType;
import ccc.chess.objects.Piece;
import ccc.chess.objects.Tile;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

@SuppressWarnings({"unused", "Duplicates", "unchecked", "WeakerAccess"})
public class GameManager {

    private Chess                   main;
    private boolean                 ownTurn     = false;
    private boolean                 curTurn     = true;
    private boolean                 team        = false;
    private boolean                 ingame      = false;
    private boolean                 won         = false;
    private boolean                 rematch     = false;
    private int                     fullMoves   = 1;
    private Thread                  connThread  = null;
    private ArrayList<Piece>        pieces      = new ArrayList<>();
    private ArrayList<Tile>         lastMove    = new ArrayList<>();
    private Piece                   promotePiece;

    public GameManager(Chess main) {
        this.main = main;
    }

    /**
     * Setup the pieces on the board to their original state
     *
     * @param team  true  = Load the board with white tiles on the bottom
     *              false = Load the board with black tiles on the bottom
     */
    public void initBoard(boolean team) {
        this.team = team;
        this.ownTurn = team;
        this.curTurn = true;
        this.won = false;
        this.rematch = false;
        this.ingame = true;
        this.fullMoves = 1;

        // Load pieces
        pieces.clear();

        // Init board to have your team on bottom
        main.getGUIManager().setSelectedTile(null);
        main.getGUIManager().init(team);

        Tile[] tiles = main.getGUIManager().getBoardTiles();

        // Set up white pieces
        pieces.add(new Piece(PieceType.WHITE_ROOK,    true, tiles[0]));
        pieces.add(new Piece(PieceType.WHITE_KNIGHT,  true, tiles[1]));
        pieces.add(new Piece(PieceType.WHITE_BISHOP,  true, tiles[2]));
        pieces.add(new Piece(PieceType.WHITE_KING,    true, tiles[3]));
        pieces.add(new Piece(PieceType.WHITE_QUEEN,   true, tiles[4]));
        pieces.add(new Piece(PieceType.WHITE_BISHOP,  true, tiles[5]));
        pieces.add(new Piece(PieceType.WHITE_KNIGHT,  true, tiles[6]));
        pieces.add(new Piece(PieceType.WHITE_ROOK,    true, tiles[7]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[8]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[9]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[10]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[11]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[12]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[13]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[14]));
        pieces.add(new Piece(PieceType.WHITE_PAWN,    true, tiles[15]));
        
        // Set up black pieces
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[48]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[49]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[50]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[51]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[52]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[53]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[54]));
        pieces.add(new Piece(PieceType.BLACK_PAWN,    false, tiles[55]));
        pieces.add(new Piece(PieceType.BLACK_ROOK,    false, tiles[56]));
        pieces.add(new Piece(PieceType.BLACK_KNIGHT,  false, tiles[57]));
        pieces.add(new Piece(PieceType.BLACK_BISHOP,  false, tiles[58]));
        pieces.add(new Piece(PieceType.BLACK_KING,    false, tiles[59]));
        pieces.add(new Piece(PieceType.BLACK_QUEEN,   false, tiles[60]));
        pieces.add(new Piece(PieceType.BLACK_BISHOP,  false, tiles[61]));
        pieces.add(new Piece(PieceType.BLACK_KNIGHT,  false, tiles[62]));
        pieces.add(new Piece(PieceType.BLACK_ROOK,    false, tiles[63]));

        gatherPossibleMoves(pieces);

        if (main.getConnManager().getClient() != null) {
            if (connThread != null)
                connThread = null;
            connThread = new Thread(main.getConnManager());
            connThread.start();
        }

        lastMove.clear();
        System.out.println("GameBoard Initialized!");
    }

    /**
     * Get a list of all the pieces currently on the board
     * @return ArrayList filled with all pieces
     */
    public ArrayList<Piece> getAllPieces() {
        return this.pieces;
    }

    /**
     * Get a list of all the pieces currently on the board
     * @param color The color of the teams pieces
     * @return ArrayList filled with all pieces
     */
    public ArrayList<Piece> getAllPieces(boolean color) {
        ArrayList<Piece> result = new ArrayList<>();
        for (Piece piece : getAllPieces()) {
            if (piece.getColor() == color) {
                result.add(piece);
            }
        }
        return result;
    }

    /**
     * Setup every pieces possibleMoves based on the provided boardState
     * @param boardState The current state of pieces currently on the board
     */
    public void gatherPossibleMoves(ArrayList<Piece> boardState) {
        for (Piece piece : boardState) {

            ArrayList<Tile> moves = new ArrayList<>();
            int index = piece.getTile().getIndex();
            Tile[] tiles = main.getGUIManager().getBoardTiles();

            HashMap<Tile, Piece> whitePieces = new HashMap<>();
            HashMap<Tile, Piece> blackPieces = new HashMap<>();

            for (Piece p : boardState) {
                if (p.getColor()) {
                    whitePieces.put(tiles[p.getTile().getIndex()], p);
                } else {
                    blackPieces.put(tiles[p.getTile().getIndex()], p);
                }
            }

            switch (piece.getType()) {
                case BLACK_KING:
                    kingCheck(moves, blackPieces, whitePieces, tiles, index);
                    break;
                case WHITE_KING:
                    kingCheck(moves, whitePieces, blackPieces, tiles, index);
                    break;
                case BLACK_QUEEN:
                    queenCheck(moves, blackPieces, whitePieces, tiles, index);
                    break;
                case WHITE_QUEEN:
                    queenCheck(moves, whitePieces, blackPieces, tiles, index);
                    break;
                case BLACK_BISHOP:
                    bishopCheck(moves, blackPieces, whitePieces, tiles, index);
                    break;
                case WHITE_BISHOP:
                    bishopCheck(moves, whitePieces, blackPieces, tiles, index);
                    break;
                case BLACK_KNIGHT:
                    knightCheck(moves, blackPieces, whitePieces, tiles, index);
                    break;
                case WHITE_KNIGHT:
                    knightCheck(moves, whitePieces, blackPieces, tiles, index);
                    break;
                case BLACK_ROOK:
                    rookCheck(moves, blackPieces, whitePieces, tiles, index);
                    break;
                case WHITE_ROOK:
                    rookCheck(moves, whitePieces, blackPieces, tiles, index);
                    break;
                case BLACK_PAWN:
                    pawnCheck(moves, piece, blackPieces, whitePieces, tiles, index);
                    break;
                case WHITE_PAWN:
                    pawnCheck(moves, piece, whitePieces, blackPieces, tiles, index);
                    break;
            }

            piece.setPossibleMoves(moves);

            if (piece.getColor()) {
                piece.setControllable((team && !piece.getPossibleMoves().isEmpty()));
            } else {
                piece.setControllable((!team && !piece.getPossibleMoves().isEmpty()));
            }
        }
    }

    /**
     * Check if a Piece at a Tile location will result in a check on own king
     * (A clone of the board is made and used to compare these changes)
     *
     * @return  true  = check
     *          false = no check
     */
    public boolean checkOwnCheck(Piece piece, Tile newLocation) {
        Tile oldLocation = piece.getTile();
        Piece tmpPiece = new Piece(piece.getType(), piece.getColor(), newLocation);
        ArrayList<Piece> piecesCopy = (ArrayList<Piece>) pieces.clone();
        piecesCopy.remove(piece);
        piecesCopy.add(tmpPiece);
        if (getPiece(newLocation) != null)
            piecesCopy.remove(getPiece(newLocation));


        // Setup possible moves for newLocation
        gatherPossibleMoves(piecesCopy);

        // Get own king
        Piece king = null;
        for (Piece p : piecesCopy) {
            if (p.getColor() == piece.getColor()) {
                if (p.isKing()) {
                    king = p;
                    break;
                }
            }
        }

        // Long live the king
        if (king == null) return true;

        // See if any opposing team have possible moves on king
        for (Piece p : piecesCopy) {
            if (p.getColor() != piece.getColor()) {
                if (p.getPossibleMoves().contains(king.getTile())) {
                    // Allow kings to move on pawns forward position (non-attack)
                    if (p.getType() == PieceType.WHITE_PAWN || p.getType() == PieceType.BLACK_PAWN) {
                        if (newLocation.getX() == p.getTile().getX()) {
                            continue;
                        }
                    }
                    // System.out.println(king.getType().name() + " can be attacked from tile #" + p.getTile().getIndex());
                    // Set moves back to default state
                    gatherPossibleMoves(getAllPieces());
                    return true;
                }
            }
        }

        // Set moves back to default state
        gatherPossibleMoves(getAllPieces());
        return false;
    }

    /**
     * Check if a Piece at a Tile location will result in a check on other team's king
     * (A clone of the board is made and used to compare these changes)
     *
     * @return  true  = check
     *          false = no check
     */
    public boolean checkOtherCheck(Piece piece, Tile newLocation) {
        Tile oldLocation = piece.getTile();
        Piece tmpPiece = new Piece(piece.getType(), piece.getColor(), newLocation);
        ArrayList<Piece> piecesCopy = (ArrayList<Piece>) pieces.clone();
        piecesCopy.remove(piece);
        piecesCopy.add(tmpPiece);
        if (getPiece(newLocation) != null)
            piecesCopy.remove(getPiece(newLocation));

        // Setup possible moves for newLocation
        gatherPossibleMoves(piecesCopy);

        // Get other king
        Piece king = null;
        for (Piece p : piecesCopy) {
            if (p.getColor() != piece.getColor()) {
                if (p.isKing()) {
                    king = p;
                    break;
                }
            }
        }

        // Long live the king
        if (king == null) return true;

        // See if any of team have possible moves on their king
        for (Piece p : piecesCopy) {
            if (p.getColor() == piece.getColor()) {
                if (p.getPossibleMoves().contains(king.getTile())) {
                    // Allow kings to move on pawns forward position (non-attack)
                    if (p.getType() == PieceType.WHITE_PAWN || p.getType() == PieceType.BLACK_PAWN) {
                        if (newLocation.getX() == p.getTile().getX()) {
                            continue;
                        }
                    }
                    // System.out.println(king.getType().name() + " can be attacked from tile #" + p.getTile().getIndex());
                    // Set moves back to default state
                    gatherPossibleMoves(getAllPieces());
                    return true;
                }
            }
        }

        // Set moves back to default state
        gatherPossibleMoves(getAllPieces());
        return false;
    }

    /**
     * Check for a check-mate on provided King Piece
     * @param king King used to check for check-mate
     * @return  true  = check-mate
     *          false = no check-mate
     */
    public boolean checkCheckMate(Piece king) {
        ArrayList<Tile> ownPossibleMoves = new ArrayList<>();
        for (Piece piece : getAllPieces()) {
            if (piece.getColor() == king.getColor()) {
                for (Tile tile : piece.getPossibleMoves()) {
                    if (!checkOwnCheck(piece, tile)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * ATTEMPT to move a Piece to a new Tile
     * (Piece will not move if a check is made possible from movement)
     *
     * @param piece The Piece being moved
     * @param location The new Tile the Piece is moving to
     */
    public void movePiece(Piece piece, Tile location) {
        System.out.println(piece.getType().name() + " moving from " + piece.getTile().getCoord() + " to " + location.getCoord());

        // Make sure king isn't in danger??
        if (checkOwnCheck(piece, location)) {
            System.out.println("CANNOT MOVE THERE! KING WOULD BE IN CHECK!");
            return;
        }

        // Remove any old pieces that were there
        if (getPiece(location) != null) {
            pieces.remove(getPiece(location));
        }

        // Update piece's location
        Tile oldLocation = piece.getTile();
        piece.setTile(location);
        piece.setNewLocation(new Point(location.getX(), location.getY()));
        piece.setMoved(true);

        // Play move sounds
        main.getSoundManager().playSound("move.mp3");

        // Set lastMove
        lastMove.add(0, piece.getTile());
        lastMove.add(1, oldLocation);

        // Upgrade pawns when appropriate
        int yIndex = piece.getTile().getIndex() / 8;
        if (piece.getType() == PieceType.WHITE_PAWN && yIndex == 7
                || piece.getType() == PieceType.BLACK_PAWN && yIndex == 0) {
            promotePiece = piece;
            if (piece.getColor() == getTeam()) {
                main.getGUIManager().setActivePanel(PanelType.UPGRADE, TransitionType.FADE_OUT, TransitionType.FADE_IN);
            } else {
                // Bot that made the move
                // Auto-promote to queen
                if (main.getConnManager().getClient() == null) {
                    promote((piece.getColor()) ? PieceType.WHITE_QUEEN : PieceType.BLACK_QUEEN);
                }
            }
            return;
        }

        setNextTurn();
    }

    /**
     *  Get the possible moves available for a king Piece
     */
    private void kingCheck(ArrayList<Tile> moves, HashMap<Tile, Piece> ownPieces, HashMap<Tile, Piece> otherPieces, Tile[] tiles, int index) {
        int yLevel = index / 8;
        
        // Horizontal
        if (index + 1 < tiles.length && ((index + 1) / 8 == yLevel)) {
            if (!ownPieces.containsKey(tiles[index + 1])) {
                moves.add(tiles[index + 1]);
            }
        }
        if (index - 1 >= 0 && ((index - 1) / 8 == yLevel)) {
            if (!ownPieces.containsKey(tiles[index  - 1])) {
                moves.add(tiles[index - 1]);
            }
        }

        // Vertical
        if (index + 8 < tiles.length && ((index + 8) / 8 == yLevel + 1)) {
            if (!ownPieces.containsKey(tiles[index  + 8])) {
                moves.add(tiles[index + 8]);
            }
        }
        if (index - 8 >= 0 && ((index - 8) / 8 == yLevel - 1)) {
            if (!ownPieces.containsKey(tiles[index  - 8])) {
                moves.add(tiles[index - 8]);
            }
        }

        // Diagonal
        if (index + 9 < tiles.length && ((index + 9) / 8 == yLevel + 1)) {
            if (!ownPieces.containsKey(tiles[index  + 9])) {
                moves.add(tiles[index + 9]);
            }
        }
        if (index - 9 >= 0 && ((index - 9) / 8 == yLevel - 1)) {
            if (!ownPieces.containsKey(tiles[index  - 9])) {
                moves.add(tiles[index - 9]);
            }
        }
        if (index + 7 < tiles.length && ((index + 7) / 8 == yLevel + 1)) {
            if (!ownPieces.containsKey(tiles[index  + 7])) {
                moves.add(tiles[index + 7]);
            }
        }
        if (index - 7 >= 0 && ((index - 7) / 8 == yLevel - 1)) {
            if (!ownPieces.containsKey(tiles[index  - 7])) {
                moves.add(tiles[index - 7]);
            }
        }

        // Castling
        boolean movedKing = true;
        for (Piece piece : ownPieces.values()) {
            if (piece.isKing()) {
                movedKing = piece.isMoved();
                break;
            }
        }
        if (!movedKing) {
            for (Piece piece : ownPieces.values()) {
                if (piece.getType() == PieceType.WHITE_ROOK
                        || piece.getType() == PieceType.BLACK_ROOK) {
                    if (!piece.isMoved()) {
                        int rookSide = piece.getTile().getIndex() % 8;
                        // Queen side
                        if (rookSide == 0) {

                        }

                        // King side
                        else if (rookSide == 7) {

                        }
                    }
                }
            }
        }
    }

    /**
     *  Get the possible moves available for a queen Piece
     */
    private void queenCheck(ArrayList<Tile> moves, HashMap<Tile, Piece> ownPieces, HashMap<Tile, Piece> otherPieces, Tile[] tiles, int index) {
        int i;
        int yLevel = index / 8;

        // Left-Up
        i = 9;
        while ((index - i >= 0 && ((index - i) % 8 < 7))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i += 9;
        }

        // Right-Down
        i = 9;
        while ((index + i < tiles.length && ((index + i) % 8 > 0))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i += 9;
        }

        // Right-Up
        i = 7;
        while ((index - i >= 0 && ((index - i) % 8 > 0))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i += 7;
        }

        // Left-Down
        i = 7;
        while ((index + i < tiles.length && ((index + i) % 8 < 7))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i += 7;
        }

        // Left
        i = 1;
        while ((index - i >= 0 && ((index - i) / 8 == yLevel))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i++;
        }

        // Right
        i = 1;
        while ((index + i < tiles.length && ((index + i) / 8 == yLevel))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i++;
        }

        // Up
        i = 8;
        while ((index - i >= 0 && ((index - i) / 8 < yLevel))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i += 8;
        }

        // Down
        i = 8;
        while ((index + i < tiles.length && ((index + i) / 8 > yLevel))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i += 8;
        }
    }

    /**
     *  Get the possible moves available for a bishop Piece
     */
    private void bishopCheck(ArrayList<Tile> moves, HashMap<Tile, Piece> ownPieces, HashMap<Tile, Piece> otherPieces, Tile[] tiles, int index) {
        int i;
        
        // Left-Up
        i = 9;
        while ((index - i >= 0 && ((index - i) % 8 < 7))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i += 9;
        }

        // Right-Down
        i = 9;
        while ((index + i < tiles.length && ((index + i) % 8 > 0))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i += 9;
        }

        // Right-Up
        i = 7;
        while ((index - i >= 0 && ((index - i) % 8 > 0))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i += 7;
        }

        // Left-Down
        i = 7;
        while ((index + i < tiles.length && ((index + i) % 8 < 7))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i += 7;
        }
    }

    /**
     *  Get the possible moves available for a knight Piece
     */
    private void knightCheck(ArrayList<Tile> moves, HashMap<Tile, Piece> ownPieces, HashMap<Tile, Piece> otherPieces, Tile[] tiles, int index) {
        int yLevel = index / 8;
        
        // Vertical
        if (index - 15 >= 0 && ((index - 15) / 8 == yLevel - 2)) {
            if (!(ownPieces.containsKey(tiles[index - 15]))) {
                moves.add(tiles[index - 15]);
            }
        }
        if (index - 17 >= 0 && ((index - 17) / 8 == yLevel - 2)) {
            if (!(ownPieces.containsKey(tiles[index - 17]))) {
                moves.add(tiles[index - 17]);
            }
        }
        if (index + 15 < tiles.length && ((index + 15) / 8 == yLevel + 2)) {
            if (!(ownPieces.containsKey(tiles[index + 15]))) {
                moves.add(tiles[index + 15]);
            }
        }
        if (index + 17 < tiles.length && ((index + 17) / 8 == yLevel + 2)) {
            if (!(ownPieces.containsKey(tiles[index + 17]))) {
                moves.add(tiles[index + 17]);
            }
        }

        // Horizontal
        if (index - 6 >= 0 && ((index - 6) / 8 == yLevel - 1)) {
            if (!(ownPieces.containsKey(tiles[index - 6]))) {
                moves.add(tiles[index - 6]);
            }
        }
        if (index - 10 >= 0 && ((index - 10) / 8 == yLevel - 1)) {
            if (!(ownPieces.containsKey(tiles[index - 10]))) {
                moves.add(tiles[index - 10]);
            }
        }
        if (index + 6 < tiles.length && ((index + 6) / 8 == yLevel + 1)) {
            if (!(ownPieces.containsKey(tiles[index + 6]))) {
                moves.add(tiles[index + 6]);
            }
        }
        if (index + 10 < tiles.length && ((index + 10) / 8 == yLevel + 1)) {
            if (!(ownPieces.containsKey(tiles[index + 10]))) {
                moves.add(tiles[index + 10]);
            }
        }
    }
    /**
     *  Get the possible moves available for a rook Piece
     */
    private void rookCheck(ArrayList<Tile> moves, HashMap<Tile, Piece> ownPieces, HashMap<Tile, Piece> otherPieces, Tile[] tiles, int index) {
        int i;
        int yLevel = index / 8;
        
        // Left
        i = 1;
        while ((index - i >= 0 && ((index - i) / 8 == yLevel))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i++;
        }

        // Right
        i = 1;
        while ((index + i < tiles.length && ((index + i) / 8 == yLevel))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i++;
        }

        // Up
        i = 8;
        while ((index - i >= 0 && ((index - i) / 8 < yLevel))
                && !ownPieces.containsKey(tiles[index - i])) {
            moves.add(tiles[index - i]);
            if (otherPieces.containsKey(tiles[index - i])) break;
            i += 8;
        }

        // Down
        i = 8;
        while ((index + i < tiles.length && ((index + i) / 8 > yLevel))
                && !ownPieces.containsKey(tiles[index + i])) {
            moves.add(tiles[index + i]);
            if (otherPieces.containsKey(tiles[index + i])) break;
            i += 8;
        }
    }

    /**
     *  Get the possible moves available for a pawn Piece
     */
    private void pawnCheck(ArrayList<Tile> moves, Piece piece, HashMap<Tile, Piece> ownPieces, HashMap<Tile, Piece> otherPieces, Tile[] tiles, int index) {

        int yLevel = index / 8;

        if (piece.getColor()) {
            // Vertical
            if (index + 8 < tiles.length) {
                if (getPiece(tiles[index + 8]) == null) {
                    moves.add(tiles[index + 8]);
                }
            }

            // Allow for 2 forward on first move
            if (!piece.isMoved()) {
                if (index + 16 < tiles.length) {
                    if (getPiece(tiles[index + 8]) == null && getPiece(tiles[index + 16]) == null) {
                        moves.add(tiles[index + 16]);
                    }
                }
            }

            // Diagonal/Vertical Attack
            if (index + 7 < tiles.length && ((index + 7) / 8 == yLevel + 1)) {
                if (otherPieces.containsKey(tiles[index + 7])) {
                    moves.add(tiles[index + 7]);
                }
            }
            if (index + 9 < tiles.length && ((index + 9) / 8 == yLevel + 1)) {
                if (otherPieces.containsKey(tiles[index + 9])) {
                    moves.add(tiles[index + 9]);
                }
            }
        } else {
            // Vertical
            if (index - 8 >= 0) {
                if (getPiece(tiles[index - 8]) == null) {
                    moves.add(tiles[index - 8]);
                }
            }

            // Allow for 2 forward on first move
            if (!piece.isMoved()) {
                if (index - 16 >= 0) {
                    if (getPiece(tiles[index - 8]) == null && getPiece(tiles[index - 16]) == null) {
                        moves.add(tiles[index - 16]);
                    }
                }
            }

            // Diagonal/Vertical Attack
            if (index - 7 >= 0 && ((index - 7) / 8 == yLevel - 1)) {
                if (otherPieces.containsKey(tiles[index - 7])) {
                    moves.add(tiles[index - 7]);
                }
            }

            if (index - 9 >= 0 && ((index - 9) / 8 == yLevel - 1)) {
                if (otherPieces.containsKey(tiles[index - 9])) {
                    moves.add(tiles[index - 9]);
                }
            }
        }
    }

    /**
     * Get a Piece object from a board Tile reference
     * @param tile Tile from GUIManager's getBoardTiles()
     * @return Piece associated with provided Tile
     */
    public Piece getPiece(Tile tile) {
        for (Piece piece : getAllPieces()) {
            if (piece.getTile() == tile) {
                return piece;
            }
        }
        return null;
    }

    /**
     * Set the screen when a check-mate is made
     * @param won Who won the game? (white=true,black=false)
     */
    public void setCheckMate(boolean won) {
        this.setWon(won);
        this.setInGame(false);
        main.getGUIManager().setSelectedTile(null);
        main.getGUIManager().setActivePanel(PanelType.INTERMISSION, TransitionType.FADE_OUT, TransitionType.FADE_IN);
        main.getGUIManager().getBackgroundPanel().setBlurred(true);
    }

    public void promote(PieceType newType) {
        promotePiece.setType(newType);
        setNextTurn();
    }

    public void setNextTurn() {
        gatherPossibleMoves(getAllPieces());

        // Check for check-mate
        for (Piece p : getAllPieces()) {
            if (p.isKing() && checkCheckMate(p)) {
                setCheckMate(!(p.getColor() == getTeam()));
                System.out.println("CHECK-MATE #4");
                return;
            }
        }

        // Change turns
        main.getGUIManager().setSelectedTile(null);
        ownTurn = !ownTurn;
        curTurn = !curTurn;

        // Black has moved
        if (curTurn) {
            fullMoves += 1;
        }

        main.getBotManager().setBotMoveTime(System.currentTimeMillis() + (new Random().nextInt(1500) + 1000));
    }

    public PieceType getTypeID(byte id) {
        switch (id) {
            case 0:
                return PieceType.BLACK_ROOK;
            case 1:
                return PieceType.BLACK_KNIGHT;
            case 2:
                return PieceType.BLACK_BISHOP;
            case 3:
                return PieceType.BLACK_QUEEN;
            case 4:
                return PieceType.WHITE_ROOK;
            case 5:
                return PieceType.WHITE_KNIGHT;
            case 6:
                return PieceType.WHITE_BISHOP;
            case 7:
                return PieceType.WHITE_QUEEN;
            default: return null;
        }
    }

    public boolean isInGame() {
        return ingame;
    }

    public void setInGame(boolean ingame) {
        this.ingame = ingame;
    }

    public boolean getTeam() {
        return team;
    }

    public void setTeam(boolean team) {
        this.team = team;
    }

    public boolean isTurn() {
        return ownTurn;
    }

    public void setIsTurn(boolean turn) {
        this.ownTurn = turn;
    }

    public boolean getCurTurn() {
        return curTurn;
    }

    public boolean won() {
        return this.won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public void setRematch(boolean rematch) {
        this.rematch = rematch;
    }

    public boolean isRematch() {
        return rematch;
    }

    public int getFullMoves() {
        return fullMoves;
    }

    public ArrayList<Tile> getLastMove() {
        return lastMove;
    }

    public Piece getPromotePiece() {
        return promotePiece;
    }
}