package ccc.chess.managers;

import ccc.chess.Chess;
import ccc.chess.objects.Piece;
import ccc.chess.objects.Tile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

import static ccc.chess.enums.PieceType.*;

public class BotManager {

    private Chess       main;
    private boolean     makingMove;
    private long        botMoveTime;

    public BotManager(Chess main) {
        this.main = main;
    }

    public void bot() {
        if (!main.getGameManager().isTurn() && !makingMove) {
            if (System.currentTimeMillis() >= botMoveTime) {
                makingMove = true;
                new Thread(() -> {
                    try {
                        String bestMove = getBestMove();
                        String coordFrom = bestMove.split("")[0] + bestMove.split("")[1];
                        String coordTo = bestMove.split("")[2] + bestMove.split("")[3];
                        int indexFrom = main.getGUIManager().getTile(coordFrom).getIndex();
                        int indexTo = main.getGUIManager().getTile(coordTo).getIndex();

                        Piece piece = main.getGameManager().getPiece(main.getGUIManager().getBoardTiles()[indexFrom]);
                        Tile newLoc = main.getGUIManager().getBoardTiles()[indexTo];
                        if (piece != null && newLoc != null) {
                            main.getGameManager().movePiece(piece, newLoc);
                        }
                        makingMove = false;
                    } catch (Exception e) {
                        makingMove = false;
                        bot();
                    }
                }).start();
            }
        }
    }

    private String getFEN() {
        StringBuilder fenBuilder = new StringBuilder();
        ArrayList<Tile> reverseList = new ArrayList<>(Arrays.asList(main.getGUIManager().getBoardTiles()));
        Collections.reverse(reverseList);
        int spaceCount = 0;
        for (Tile tile : reverseList) {
            Piece piece = main.getGameManager().getPiece(tile);
            if (piece != null) {

                if (spaceCount > 0) {
                    fenBuilder.append(spaceCount);
                    spaceCount = 0;
                }

                // set pieces
                if (piece.getType() == WHITE_KING) {
                    fenBuilder.append("K");
                } else if (piece.getType() == WHITE_QUEEN) {
                    fenBuilder.append("Q");
                } else if (piece.getType() == WHITE_BISHOP) {
                    fenBuilder.append("B");
                } else if (piece.getType() == WHITE_KNIGHT) {
                    fenBuilder.append("N");
                } else if (piece.getType() == WHITE_ROOK) {
                    fenBuilder.append("R");
                } else if (piece.getType() == WHITE_PAWN) {
                    fenBuilder.append("P");
                } else if (piece.getType() == BLACK_KING) {
                    fenBuilder.append("k");
                } else if (piece.getType() == BLACK_QUEEN) {
                    fenBuilder.append("q");
                } else if (piece.getType() == BLACK_BISHOP) {
                    fenBuilder.append("b");
                } else if (piece.getType() == BLACK_KNIGHT) {
                    fenBuilder.append("n");
                } else if (piece.getType() == BLACK_ROOK) {
                    fenBuilder.append("r");
                } else if (piece.getType() == BLACK_PAWN) {
                    fenBuilder.append("p");
                }
            } else {
                spaceCount += 1;
            }

            // Check if at end of line
            if (tile.getIndex() % 8 == 0
                    && tile.getIndex() % 64 != 0) {
                if (spaceCount > 0) {
                    fenBuilder.append(spaceCount);
                    spaceCount = 0;
                }

                fenBuilder.append("/");
            }
        }

        // Current Turn
        if (main.getGameManager().getCurTurn()) {
            fenBuilder.append(" w");
        } else {
            fenBuilder.append(" b");
        }

        // Castling
        fenBuilder.append(" -");

        // En passant
        fenBuilder.append(" -");

        // Halfmove clock
        fenBuilder.append(" 0");

        // Full moves mad
        fenBuilder.append(" ");
        fenBuilder.append(main.getGameManager().getFullMoves());

        return fenBuilder.toString();
    }

    public String getBestMove() {
        String result = "";
        try {
            URL apiURL = new URL("http://lab46.g7n.org/~dschmitt/stockfish.php");
            HttpURLConnection urlConnection = (HttpURLConnection) apiURL.openConnection();

            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            Map<String,String> arguments = new HashMap<>();
            String fen = getFEN();
            arguments.put("fen", fen);
            arguments.put("skill", "0");
            arguments.put("sleep", "0.75");
            StringBuilder sj = new StringBuilder();
            for(Map.Entry<String,String> entry : arguments.entrySet()) {
                sj.append(URLEncoder.encode(entry.getKey(), "UTF-8")).append("=").append(URLEncoder.encode(entry.getValue(), "UTF-8")).append("&");
            }
            byte[] out = sj.toString().getBytes();

            urlConnection.setFixedLengthStreamingMode(out.length);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            urlConnection.connect();

            // Write to output stream
            OutputStream os = urlConnection.getOutputStream();
            os.write(out);

            // Get response
            InputStream is = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            result = br.readLine();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({"Duplicates", "unused"})
    private void botCheck() {
        if (main.getGameManager().isInGame()) {
            Piece king = null;
            boolean isCheck = false;
            boolean ownColor = !main.getGameManager().getTeam();

            for (Piece piece : main.getGameManager().getAllPieces()) {
                if ((piece.getColor() == ownColor) && piece.isKing()) {

                    // Check for check
                    if (main.getGameManager().checkOwnCheck(piece, piece.getTile())) {
                        isCheck = true;
                    }

                    king = piece;
                    break;
                }
            }

            if (isCheck) {
                System.out.println("CHECK");

                // See if king can kill it
                for (Tile tile : king.getPossibleMoves()) {
                    if (main.getGameManager().getPiece(tile) != null) {
                        if (!main.getGameManager().checkOwnCheck(king, tile)) {
                            main.getGameManager().movePiece(king, tile);
                            return;
                        }
                    }
                }

                // See if anything can kill it (not king)
                for (Piece piece : main.getGameManager().getAllPieces(ownColor)) {
                    if (piece != king) {
                        for (Tile tile : piece.getPossibleMoves()) {
                            if (main.getGameManager().getPiece(tile) != null) {
                                if (!main.getGameManager().checkOwnCheck(piece, tile)) {
                                    main.getGameManager().movePiece(piece, tile);
                                    return;
                                }
                            }
                        }
                    }
                }

                // See if king can kill it
                for (Tile tile : king.getPossibleMoves()) {
                    if (!main.getGameManager().checkOwnCheck(king, tile)) {
                        main.getGameManager().movePiece(king, tile);
                        return;
                    }
                }

                // See if any can move into the way
                for (Piece piece : main.getGameManager().getAllPieces(ownColor)) {
                    if (piece != king) {
                        for (Tile tile : piece.getPossibleMoves()) {
                            if (!main.getGameManager().checkOwnCheck(piece, tile)) {
                                main.getGameManager().movePiece(piece, tile);
                                return;
                            }
                        }
                    }
                }

                return;
            }

            // Get all possible moves/attacks
            HashMap<Piece, List<Tile>> allPossibleAttacks = new HashMap<>();
            HashMap<Piece, List<Tile>> allPossibleMoves = new HashMap<>();

            for (Piece piece : main.getGameManager().getAllPieces(ownColor)) {
                if (!piece.getPossibleMoves().isEmpty()) {
                    for (Tile tile : piece.getPossibleMoves()) {
                        if (!main.getGameManager().checkOwnCheck(piece, tile)) {
                            if (main.getGameManager().getPiece(tile) != null) {
                                allPossibleAttacks.computeIfAbsent(piece, k -> new ArrayList<>());
                                allPossibleAttacks.get(piece).add(tile);
                            } else {
                                allPossibleMoves.computeIfAbsent(piece, k -> new ArrayList<>());
                                allPossibleMoves.get(piece).add(tile);
                            }
                        }
                    }
                }
            }

            if (!allPossibleAttacks.isEmpty()) {
                // Only attack 90% of the time (for now)
                if ((new Random().nextInt(100) + 1) > 10) {
                    Piece piece = (Piece) allPossibleAttacks.keySet().toArray()[new Random().nextInt(allPossibleAttacks.size())];
                    main.getGameManager().movePiece(piece, allPossibleAttacks.get(piece).get(new Random().nextInt(allPossibleAttacks.get(piece).size())));
                    return;
                }
            }

            if (!allPossibleMoves.isEmpty()) {
                // Go for check 90% of the time (for now)
                if ((new Random().nextInt(100) + 1) > 10) {
                    for (Piece piece : allPossibleMoves.keySet()) {
                        for (Tile tile : allPossibleMoves.get(piece)) {
                            if (!main.getGameManager().checkOwnCheck(piece, tile)) {
                                if (main.getGameManager().checkOtherCheck(piece, tile)) {
                                    main.getGameManager().movePiece(piece, tile);
                                    return;
                                }
                            }
                        }
                    }
                }

                // Choose a random place to move (for now)
                // Make sure move will not compromise the king
                Piece piece = null;
                Tile tile = null;
                while ((piece == null) || main.getGameManager().checkOwnCheck(piece, tile)) {
                    piece = (Piece) allPossibleMoves.keySet().toArray()[new Random().nextInt(allPossibleMoves.size())];
                    tile = piece.getPossibleMoves().get(new Random().nextInt(piece.getPossibleMoves().size()));
                }

                main.getGameManager().movePiece(piece, tile);
            }
        }
    }

    public long getBotMoveTime() {
        return botMoveTime;
    }

    public void setBotMoveTime(long botMoveTime) {
        this.botMoveTime = botMoveTime;
    }
}
