package ccc.chess.managers;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.PieceType;
import ccc.chess.enums.TransitionType;
import ccc.chess.objects.Tile;
import ccc.chess.panels.*;
import ccc.chess.panels.core.ChessPanel;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.util.Duration;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

public class GUIManager {

    private Chess                       main;                           // Main Chess JFrame instance

    private BackgroundPanel             backgroundPanel;

    private MenuPanel                   menuPanel;                          // MenuPanel instance
    private GamePanel                   gamePanel;                          // GamePanel instance
    private SettingsPanel               settingsPanel;                      // SettingsPanel instance
    private PlayPanel                   playPanel;                          // PlayPanel instance
    private PausePanel                  pausePanel;                         // PausePanel instance
    private UpgradePanel                upgradePanel;                       // UpgradePanel instance
    private InfoPanel                   infoPanel;                          // InfoPanel instance
    private HostPanel                   hostPanel;                          // HostPanel instance
    private JoinPanel                   joinPanel;                          // JoinPanel instance
    private IntermissionPanel           intermissionPanel;                  // IntermissionPanel instance

    private ChessPanel activePanel = null;
    private Tile                        hoveredTile;                    // Mouse-Hovered Tile
    private Tile                        selectedTile;                   // Last clicked Tile
    private Tile[]                      boardTiles  = new Tile[64];     // Array of every game board Tile
    private static HashMap<PieceType, Image>   pieceImages = new HashMap<>();  // Images associated with their respective PieceType
    private String                      infoText;                       // Text being used in the InfoPanel
    
    private int                         tileMarginWidth;
    private int                         tileMarginHeight;
    private int                         tileWidth;
    private int                         tileHeight;

    public GUIManager(Chess main) {
        this.main = main;

        tileMarginWidth     = (int) ((main.getMasterPane().getWidth() / 18) + 1);
        tileMarginHeight    = (int) ((main.getMasterPane().getHeight() / 18) + 1);
        tileWidth           = (int) (((main.getMasterPane().getWidth() - (2 * tileMarginWidth)) / 8) + 1);
        tileHeight          = (int) (((main.getMasterPane().getHeight() - (2 * tileMarginHeight)) / 8) + 1);
        this.loadPieceImages();

        this.backgroundPanel    = new BackgroundPanel(main);
        this.menuPanel          = new MenuPanel(main);
        this.gamePanel          = new GamePanel(main);
        this.settingsPanel      = new SettingsPanel(main);
        this.playPanel          = new PlayPanel(main);
        this.pausePanel         = new PausePanel(main);
        this.upgradePanel       = new UpgradePanel(main);
        this.infoPanel          = new InfoPanel(main);
        this.hostPanel          = new HostPanel(main);
        this.joinPanel          = new JoinPanel(main);
        this.intermissionPanel  = new IntermissionPanel(main);
        main.getMasterPane().getChildren().addAll(
                backgroundPanel, menuPanel, playPanel, settingsPanel, gamePanel, pausePanel, intermissionPanel, upgradePanel, joinPanel, hostPanel, infoPanel);
        main.getMasterPane().getChildren().forEach(child -> {
            ChessPanel cp = (ChessPanel) child;
            cp.init();
            cp.setupMouseHandler();
            cp.setupKeyHandler();
        });
    }

    /**
     * Set the active Panel on the MasterPanel
     * (Setting the active panel calls the init() function on that Panel)
     *
     * @param type  The PanelType that is being set active
     * @param transOut Type of transition for the panel leaving
     * @param transIn Type of transition for the panel entering
     */
    public void setActivePanel(PanelType type, TransitionType transOut, TransitionType transIn) {
        this.setActivePanel(type, transOut, transIn, 250);
    }

    /**
     * Set the active Panel on the MasterPanel
     * (Setting the active panel calls the init() function on that Panel)
     *
     * @param type  The PanelType that is being set active
     * @param transOut Type of transition for the panel leaving
     * @param transIn Type of transition for the panel entering
     * @param transSpeed The speed of transition (in milliseconds)
     */
    public void setActivePanel(PanelType type, TransitionType transOut, TransitionType transIn, int transSpeed) {
        Platform.runLater(() -> {
            /* Play "OUT" Animation */
            if (activePanel != null) {
                if (type == activePanel.getType()) return;
                doTransition(activePanel, transOut, transSpeed);
            }

            /* Set activePanel */
            switch (type) {
                case MENU:
                    this.activePanel = menuPanel;
                    break;
                case PLAY:
                    this.activePanel = playPanel;
                    break;
                case JOIN:
                    this.activePanel = joinPanel;
                    break;
                case HOST:
                    this.activePanel = hostPanel;
                    break;
                case SETTINGS:
                    this.activePanel = settingsPanel;
                    break;
                case GAME:
                    this.activePanel = gamePanel;
                    break;
                case PAUSE:
                    this.activePanel = pausePanel;
                    break;
                case INTERMISSION:
                    this.activePanel = intermissionPanel;
                    this.activePanel.init();
                    break;
                case UPGRADE:
                    this.activePanel = upgradePanel;
                    this.activePanel.init();
                    break;
                case INFO:
                    this.activePanel = infoPanel;
                    break;
                default:
                    this.activePanel = menuPanel;
                    this.backgroundPanel.setBlurred(true);
                    break;
            }
            /* Setup Active Panel */
            this.activePanel.setOpacity(1.0F);
            this.activePanel.setVisible(true);
            this.activePanel.requestFocus();

            /* Play "IN" Animation */
            doTransition(activePanel, transIn, transSpeed);
        });
    }

    /**
     * Handle transitions between panels
     * @param panel The panel being transitioned
     * @param type The type of transition being applied
     * @param speed The speed of transition (milliseconds)
     */
    private void doTransition(ChessPanel panel, TransitionType type, int speed) {
        Animation anim = null;
        int width = (int) panel.getWidth();

        switch (type) {
            case NONE_IN:
                anim = new TranslateTransition(Duration.millis(speed), panel);
                ((TranslateTransition) anim).setToX(0);
                ((TranslateTransition) anim).setFromX(0);
                anim.setOnFinished(e -> hideOtherPanels());
                break;
            case NONE_OUT:
                anim = new TranslateTransition(Duration.millis(speed), panel);
                ((TranslateTransition) anim).setToX(0);
                ((TranslateTransition) anim).setFromX(0);
                break;
            case SLIDE_IN_LEFT:
                anim = new TranslateTransition(Duration.millis(speed), panel);
                ((TranslateTransition) anim).setToX(0);
                ((TranslateTransition) anim).setFromX(-width);
                anim.setOnFinished(e -> hideOtherPanels());
                break;
            case SLIDE_OUT_LEFT:
                anim = new TranslateTransition(Duration.millis(speed), panel);
                ((TranslateTransition) anim).setToX(-width);
                ((TranslateTransition) anim).setFromX(0);
                break;
            case SLIDE_IN_RIGHT:
                anim = new TranslateTransition(Duration.millis(speed), panel);
                ((TranslateTransition) anim).setToX(0);
                ((TranslateTransition) anim).setFromX(width);
                anim.setOnFinished(e -> hideOtherPanels());
                break;
            case SLIDE_OUT_RIGHT:
                anim = new TranslateTransition(Duration.millis(speed), panel);
                ((TranslateTransition) anim).setToX(width);
                ((TranslateTransition) anim).setFromX(0);
                break;
            case FADE_IN:
                anim = new FadeTransition(Duration.millis(speed), panel);
                ((FadeTransition) anim).setFromValue(0.0);
                ((FadeTransition) anim).setToValue(1.0);
                anim.setOnFinished(e -> hideOtherPanels());
                break;
            case FADE_OUT:
                anim = new FadeTransition(Duration.millis(speed), panel);
                ((FadeTransition) anim).setFromValue(1.0);
                ((FadeTransition) anim).setToValue(0.0);
                anim.setAutoReverse(true);
                break;
        }

        panel.setCurrentAnimation(anim);
        anim.play();
    }

    /**
     * Make sure only the active panel is visible
     * (Done when "IN" Animation finishes)
     */
    private void hideOtherPanels() {
        main.getMasterPane().getChildren().forEach(child -> {
            if (child != activePanel && child != backgroundPanel) {
                child.setVisible(false);
            }
        });
    }

    /**
     * Get the currently active panel
     * @return instance of the currently active panel
     */
    public ChessPanel getActivePanel() {
        return activePanel;
    }

    /**
     * Initialize GUIManager's Tiles
     *
     * @param inverted  true  = (TopLeft-BottomRight) 0-63
     *                  false = (BottomRight-TopLeft) 0-63
     */
    public void init(boolean inverted) {
        loadBoardTiles(inverted);
    }

    /**
     * Get the image associated with the specific PieceType
     *
     * @param type The type of Piece
     * @return The Image of the PieceType
     */
    public static Image getPieceImage(PieceType type) {
        return pieceImages.get(type);
    }

    /**
     * Get the array of tiles loaded in from this.init()
     * @return Tile array
     */
    public Tile[] getBoardTiles() {
        return boardTiles;
    }

    /**
     * Get the current user-selected Tile
     * @return currently selected tile
     */
    public Tile getSelectedTile() {
        return selectedTile;
    }

    /**
     * Set the current selected tile
     */
    public void setSelectedTile(Tile selectedTile) {
        this.selectedTile = selectedTile;
    }

    /**
     * Get the current hovered-over Tile
     * @return currently hovered-over tile
     */
    public Tile getHoveredTile() {
        return hoveredTile;
    }

    /**
     * Set the current hovered tile
     */
    public void setHoveredTile(Tile hoveredTile) {
        this.hoveredTile = hoveredTile;
    }

    /**
     * Get the width of a tile
     * @return global tile width
     */
    public int getTileWidth() {
        return tileWidth;
    }

    /**
     * Get the height of a tile
     * @return global tile height
     */
    public int getTileHeight() {
        return tileHeight;
    }

    /**
     * Get the text on the InfoPanel
     * @return Text set on InfoPanel
     */
    public String getInfoText() {
        return infoText;
    }

    /**
     * Set the text on the InfoPanel
     * @param infoText The text being set on the InfoPanel
     */
    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    /**
     * Get the instance of the BackgroundPanel
     * @return BackgroundPanel instance
     */
    public BackgroundPanel getBackgroundPanel() {
        return backgroundPanel;
    }

    /**
     * Load the Tiles onto the board
     * (Inverting will load the tiles in backwards)
     *
     * @param inverted true  = (TopLeft-BottomRight) 0-63
     *                 false = (BottomRight-TopLeft) 0-63
     */
    private void loadBoardTiles(boolean inverted) {
        if (inverted) {
            for (int y = 7; y >= 0; y--) {
                for (int x = 7; x >= 0; x--) {
                    boardTiles[63 - ((8 * y) + x)]
                            = new Tile(63 - ((8 * y) + x), (tileWidth * x) + tileMarginWidth, (tileHeight * y) + tileMarginHeight, tileWidth, tileHeight);
                }
            }
        } else {
            for (int y = 0; y < 8; y++) {
                for (int x = 0; x < 8; x++) {
                    boardTiles[((8 * y) + x)]
                            = new Tile(((8 * y) + x), (tileWidth * x) + tileMarginWidth, (tileHeight * y) + tileMarginHeight, tileWidth, tileHeight);
                }
            }
        }
    }

    /**
     * Load the Images associated with each Piece
     * (Images are stored in getPieceImages() HashMap)
     */
    private void loadPieceImages() {
        try {
            BufferedImage pieceSheet = ImageIO.read(getClass().getResourceAsStream("/images/chess_pieces.png"));
            int totalWidth  = pieceSheet.getWidth();
            int totalHeight = pieceSheet.getHeight();

            int x = 0, y = 0;
            for (PieceType type : PieceType.values()) {
                java.awt.Image pieceImg =
                        pieceSheet.getSubimage(
                                ((totalWidth / 6) * x),
                                ((totalHeight / 2) * y),
                                totalWidth / 6,
                                totalHeight / 2).getScaledInstance(tileWidth, tileHeight, java.awt.Image.SCALE_SMOOTH);
                BufferedImage tmp = new BufferedImage(pieceImg.getWidth(null), pieceImg.getHeight(null), BufferedImage.TYPE_INT_ARGB);
                tmp.getGraphics().drawImage(pieceImg, 0, 0, null);
                pieceImages.put(type, SwingFXUtils.toFXImage(tmp, null));
                if (x == 5) {
                    x = 0;
                    y++;
                } else {
                    x++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Tile getTile(String coord) {
        for (Tile tile : getBoardTiles()) {
            if (tile.getCoord().equalsIgnoreCase(coord)) {
                return tile;
            }
        }
        return null;
    }
}