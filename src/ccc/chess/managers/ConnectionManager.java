package ccc.chess.managers;

import ccc.chess.Chess;
import ccc.chess.enums.PanelType;
import ccc.chess.enums.PieceType;
import ccc.chess.enums.TransitionType;
import ccc.chess.objects.Piece;
import ccc.chess.objects.Tile;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ConnectionManager implements Runnable {

    private Chess main;

    private volatile Socket         client;
    private volatile ServerSocket   server;
    private          Thread         serverThread;
    private          Thread         clientThread;
    private volatile long           timeout;
    private volatile boolean        connecting;

    public ConnectionManager(Chess main) {
        this.main = main;
    }

    /**
     * Setup the Socket connection to the specified IP Address and Port
     * @param ip IP Address being connected to
     * @param port Port which current ServerSocket is running on
     */
    public void joinConnection(String ip, int port, boolean reconnect) {
        if (client != null || server != null) closeConnection();
        if (!reconnect) connecting = true;
        System.out.println("Connecting...");
        clientThread = new Thread(() -> {
            try {
                if (connecting) {
                    if (timeout == 0L) timeout = System.currentTimeMillis() + 5000L;
                    client = new Socket(ip, port);

                    try {
                        DataInputStream in = new DataInputStream(main.getConnManager().getClient().getInputStream());
                        main.getGameManager().initBoard(in.readByte() == (byte) 0);
                        main.getGameManager().setInGame(true);
                        main.getGUIManager().setSelectedTile(null);
                        main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                        main.getGUIManager().getBackgroundPanel().setBlurred(false);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        main.getConnManager().closeConnection();
                        main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
                        main.getGUIManager().getBackgroundPanel().setBlurred(true);
                    }

                } else {
                    timeout = 0L;
                }
            } catch (IOException ex) {
                System.out.println("Connection Not Successful.");
                if (server == null && System.currentTimeMillis() <= timeout) {
                    System.out.println("Attempting Reconnect...");
                    joinConnection(ip, port, true);
                } else {
                    timeout = 0L;
                    main.getGUIManager().setInfoText("Connection Failed!");
                    main.getGUIManager().setActivePanel(PanelType.INFO, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                }
            }
        });
        clientThread.start();
    }

    /**
     * Setup the ServerSocket Connection
     * (Also establishes client connection when server is ready)
     *
     * @param port The port which the ServerSocket will be running on
     */
    public void hostConnection(int port) {
        closeConnection();
        System.out.println("Hosting ServerSocket...");
        try {
            server = new ServerSocket(port);
            serverThread = new Thread(() -> {
                try {
                    client = server.accept();
                    System.out.println("Connection Established.");

                    byte team = (byte) ((new Random().nextBoolean()) ? 1 : 0);
                    try {
                        DataOutputStream out = new DataOutputStream(main.getConnManager().getClient().getOutputStream());
                        out.write(team);
                        main.getGameManager().initBoard(team == (byte) 1);
                        main.getGameManager().setInGame(true);
                        main.getGUIManager().setSelectedTile(null);
                        main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                        main.getGUIManager().getBackgroundPanel().setBlurred(false);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        main.getConnManager().closeConnection();
                        main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.SLIDE_IN_LEFT);
                        main.getGUIManager().getBackgroundPanel().setBlurred(true);
                    }

                } catch (IOException e) {
                    System.out.println("Connection Closed.");
                }
            });
            serverThread.start();
            System.out.println("Waiting For Connection...");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Close the current socket connection
     * (If server, shut down server connection)
     */
    public void closeConnection() {
        System.out.println("Closing Connection...");
        try {
            if (client != null) {
                client.close();
                client = null;
            }
            if (server != null) {
                server.close();
                server = null;
            }
            connecting   = false;
            clientThread = null;
            serverThread = null;
            System.out.println("Connection Closed.");
        } catch (IOException ignored) {}
    }

    public Socket getClient() {
        return this.client;
    }

    /**
     * Client-Socket loop
     * (Controls receiving input from the server)
     */
    @Override
    public void run() {
        while (client != null) {
            try {

                // Get Server Data
                BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
                String line;

                if ((line = input.readLine()) != null) {
                    String[] data = line.split(":");
                    String header = data[0];

                    switch (header) {
                        case "MOVE":
                            int index1 = Integer.parseInt(data[1]);
                            int index2 = Integer.parseInt(data[2]);
                            Piece piece = main.getGameManager().getPiece(main.getGUIManager().getBoardTiles()[index1]);
                            Tile newLocation = main.getGUIManager().getBoardTiles()[index2];
                            main.getGameManager().movePiece(piece, newLocation);
                            break;
                        case "PROMOTION":
                            PieceType newType = PieceType.valueOf(data[1]);
                            main.getGameManager().promote(newType);
                            break;
                        case "REMATCH":
                            if (main.getGameManager().isRematch()) {
                                main.getGameManager().initBoard(main.getGameManager().getTeam());
                                main.getGUIManager().setActivePanel(PanelType.GAME, TransitionType.FADE_OUT, TransitionType.FADE_IN, 100);
                                main.getGUIManager().getBackgroundPanel().setBlurred(false);
                            } else {
                                main.getGameManager().setRematch(true);
                            }
                            break;
                    }
                } else {
                    this.closeConnection();
                    main.getGameManager().setInGame(false);
                    main.getGUIManager().setActivePanel(PanelType.MENU, TransitionType.FADE_OUT, TransitionType.FADE_IN);
                    main.getGUIManager().getBackgroundPanel().setBlurred(true);
                }

                Thread.sleep(250);
            } catch (IOException | InterruptedException ignored) { }
        }
    }
}
