package ccc.chess.managers;

import ccc.chess.Chess;
import javafx.scene.media.AudioClip;

import java.util.HashMap;

public class SoundManager {

    private Chess main;
    private HashMap<String, AudioClip> sounds;

    public SoundManager(Chess main) {
        this.main = main;
        this.sounds = new HashMap<>();
        sounds.put("menu_click.mp3", new AudioClip(getClass().getResource("/sounds/menu_click.mp3").toExternalForm()));
        sounds.put("move.mp3", new AudioClip(getClass().getResource("/sounds/move.mp3").toExternalForm()));
    }

    public void playSound(String fileName) {
        new Thread(() -> {
            if (sounds.get(fileName) != null) {
                sounds.get(fileName).play(main.getVolume());
            } else {
                new AudioClip(getClass().getResource("/sounds/" + fileName).toExternalForm()).play(main.getVolume());
            }
        }).start();
    }
}
