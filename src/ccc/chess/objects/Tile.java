package ccc.chess.objects;

public class Tile {

    private int index;
    private int x, y, w, h;

    public Tile(int index, int x, int y, int w, int h) {
        this.index = index;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return w;
    }

    public int getHeight() {
        return h;
    }

    public int getIndex() {
        return index;
    }

    public String getCoord() {
        char c = (char) ('h' - (index % 8));
        int row = (index / 8) + 1;
        return c + "" + row;
    }
}
