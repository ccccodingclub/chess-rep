package ccc.chess.objects;

import ccc.chess.enums.PieceType;

import java.awt.*;
import java.util.ArrayList;

public class Piece {

    private PieceType       type;
    private boolean         controllable;
    private boolean         moved;
    private boolean         color;
    private Point           location;
    private Point           newLocation;
    private Tile            tile;
    private ArrayList<Tile> possibleMoves;

    public Piece(PieceType type, boolean white, Tile tile) {
        this.type = type;
        this.moved = false;
        this.color = white;
        this.possibleMoves = new ArrayList<>();
        this.tile = tile;
        this.location = new Point(tile.getX(), tile.getY());
        this.newLocation = location;
    }

    public boolean isControllable() {
        return controllable;
    }

    public void setControllable(boolean controllable) {
        this.controllable = controllable;
    }

    public ArrayList<Tile> getPossibleMoves() {
        return possibleMoves;
    }

    public void setPossibleMoves(ArrayList<Tile> possibleMoves) {
        this.possibleMoves = possibleMoves;
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    public boolean getColor() {
        return color;
    }

    public PieceType getType() {
        return type;
    }

    public void setType(PieceType type) {
        this.type = type;
    }

    public boolean isKing() {
        return (type == PieceType.WHITE_KING || type == PieceType.BLACK_KING);
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(int x, int y) {
        this.location = new Point(x, y);
    }

    public Point getNewLocation() {
        return newLocation;
    }

    public void setNewLocation(Point newLocation) {
        this.newLocation = newLocation;
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }
}
