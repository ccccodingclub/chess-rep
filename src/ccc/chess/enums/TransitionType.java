package ccc.chess.enums;

public enum  TransitionType {
    NONE_IN,
    NONE_OUT,
    SLIDE_IN_RIGHT,
    SLIDE_IN_LEFT,
    SLIDE_OUT_RIGHT,
    SLIDE_OUT_LEFT,
    FADE_IN,
    FADE_OUT
}
