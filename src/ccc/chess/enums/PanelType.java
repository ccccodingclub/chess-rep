package ccc.chess.enums;

public enum PanelType {
    GAME,
    PLAY,
    HOST,
    JOIN,
    MENU,
    INTERMISSION,
    SETTINGS,
    BACKGROUND,
    PAUSE,
    UPGRADE,
    INFO
}
